package in.nda.argue.user.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

public class InviteUserDTO {

	@NotNull(message="Enter \"invitee\" name")
	private String invitee;

	@NotNull(message="Email should not be null")
	@Email(message="Email should be well formatted")
	private String email;
	
	@NotNull(message="Role should not be null")
	private String role;

	public String getInvitee() {
		return invitee;
	}

	public void setInvitee(String invitee) {
		this.invitee = invitee;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getRole() {
		return role;
	}
	
	public void setRole(String role) {
		this.role = role;
	}

	@Override
	public String toString() {
		return "InviteUserDTO [invitee=" + invitee + ", email=" + email + ", role=" + role + "]";
	}
}
