package in.nda.argue.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArgueApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArgueApplication.class, args);
	}
}
