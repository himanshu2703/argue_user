package in.nda.argue.user.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code=HttpStatus.INTERNAL_SERVER_ERROR, reason="Unable to send mail due to some internal error.")
public class SendMailException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1497328066094440481L;

	public SendMailException() {
		super();
	}
	
	public SendMailException(String message) {
		super(message);
	}
	
	public SendMailException(Throwable exp) {
		super(exp);
	}
	
	public SendMailException(String message, Throwable exp) {
		super(message, exp);
	}
}
