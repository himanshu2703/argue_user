package in.nda.argue.user.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code=HttpStatus.INTERNAL_SERVER_ERROR, reason="User is not authorized to create user of given role.")
public class RoleException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1497328066094440481L;

	public RoleException() {
		super();
	}
	
	public RoleException(String message) {
		super(message);
	}
	
	public RoleException(Throwable exp) {
		super(exp);
	}
	
	public RoleException(String message, Throwable exp) {
		super(message, exp);
	}
}
