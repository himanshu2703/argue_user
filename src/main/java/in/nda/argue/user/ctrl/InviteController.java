package in.nda.argue.user.ctrl;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import in.nda.argue.user.dto.InviteUserDTO;
import in.nda.argue.user.model.InviteUser;
import in.nda.argue.user.service.InviteService;

/**
 * 
 * @author Himanshu
 *
 */
@CrossOrigin
@RestController
@RequestMapping(value = "/invite")
public class InviteController {

	private Logger logger = LoggerFactory.getLogger(InviteController.class);

	@Autowired
	private InviteService inviteService;

	/**
	 * 
	 * @param inviteUserDTO
	 * @return
	 */
	@PostMapping(value = "/users/{userName}", produces = MediaType.TEXT_PLAIN_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> getInvitationURL(@Valid @RequestBody InviteUserDTO inviteUserDTO,
			@PathVariable String userName) {
		logger.info("{} send invite to user = {}", userName, inviteUserDTO);
		ControllerLinkBuilder linkBuilder = ControllerLinkBuilder.linkTo(InviteController.class).slash("registration");
		String strLink = linkBuilder.toUri().toString() + "/";
		String statusMessage = inviteService.sendMailInvitation(inviteUserDTO, strLink, userName);

		return new ResponseEntity<>(statusMessage, HttpStatus.OK);
	}

	/**
	 * 
	 * @param inviteId
	 * @return
	 */
	@GetMapping(value = "/{inviteId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<InviteUser> getInvitationDetail(@PathVariable String inviteId) {
		logger.info("Fetch the invite detail based on invite id ={}", inviteId);
		InviteUser inviteUser = inviteService.getInvitationDetail(inviteId);

		return new ResponseEntity<>(inviteUser, HttpStatus.OK);
	}

	/**
	 * 
	 * @return
	 */
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<InviteUser>> getAllInvite() {
		logger.info("Fetch all the invite details.");
		List<InviteUser> listInviteUser = inviteService.getAllInvites();

		return new ResponseEntity<>(listInviteUser, HttpStatus.OK);
	}

	/**
	 * 
	 * @param inviteId
	 * @return
	 */
	@DeleteMapping(value = "/{inviteId}")
	public ResponseEntity<Void> deleteInvite(@PathVariable String inviteId) {
		logger.info("Delete the invite based on invite id ={}", inviteId);
		inviteService.deleteInvite(inviteId);

		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	/**
	 * 
	 * @param inviteId
	 * @return
	 */
	@GetMapping(value = "/registration/{token}", produces = MediaType.TEXT_PLAIN_VALUE)
	public String registrationUser(@PathVariable String token) {
		logger.info("Validate the token. If it is expired, do not let to register.");
		boolean isValidInvite = inviteService.validateInvite(token);

		if (isValidInvite) {
			logger.info("Fetching of user invite details based on token and send registration form to the user.");
			InviteUser user = inviteService.getUserFromToken(token);
			return "Register following user: " + user;
		} else {
			return "Invitation has expired";
		}
	}

}
