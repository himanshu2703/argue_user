package in.nda.argue.user.ctrl;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import in.nda.argue.user.dto.UserDTO;
import in.nda.argue.user.model.ArgueUser;
import in.nda.argue.user.service.ArgueUserService;

@CrossOrigin
@RestController
@RequestMapping(value = "/user", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
public class ArgueUserController {
	
	private Logger logger = LoggerFactory.getLogger(ArgueUserController.class);

	@Autowired
	private ArgueUserService userService;

	@GetMapping("/{userId}")
	public ResponseEntity<UserDTO> getUserByUUID(@PathVariable("userId") String userId) {
		logger.info("Getting the user based on argue user id: {}", userId);
		UserDTO user = userService.getUserByUUID(userId);

		return new ResponseEntity<>(user, HttpStatus.OK);
	}

	@GetMapping
	public ResponseEntity<List<UserDTO>> getAllUsers() {
		logger.info("Getting all users from active directory.");
		List<UserDTO> listUser = userService.getAllUsers();

		return new ResponseEntity<>(listUser, HttpStatus.OK);
	}

	@DeleteMapping("/{userId}")
	public ResponseEntity<ArgueUser> deleteUserByUUID(@PathVariable("userId") String userId) {
		logger.info("Deleting user of userId= {}", userId);
		userService.deleteArgueUser(userId);

		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@PutMapping("/{userId}")
	public ResponseEntity<UserDTO> updateArgueUser(@PathVariable("userId") String userId,
			@Valid @RequestBody ArgueUser argueUser) {
		logger.info("Updating user of userid= {} with values = {}", userId, argueUser );
		UserDTO user = userService.updateArgueUpdate(userId, argueUser);

		return new ResponseEntity<>(user, HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<UserDTO> createArgueUser(@Valid @RequestBody UserDTO userDTO) {
		logger.info("Creating of new users from data = {} ", userDTO);
		UserDTO userDto = userService.createArgueUser(userDTO);

		return new ResponseEntity<>(userDto, HttpStatus.OK);
	}

}
