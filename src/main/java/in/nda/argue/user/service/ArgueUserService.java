package in.nda.argue.user.service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.nda.argue.user.dto.UserDTO;
import in.nda.argue.user.model.ArgueUser;
import in.nda.argue.user.repo.ArgueUserRepository;

@Service
public class ArgueUserService {
	
	private Logger logger = LoggerFactory.getLogger(ArgueUserService.class);

	@Autowired
	private ArgueUserRepository userRepo;

	public UserDTO getUserByUUID(String userId) {
		logger.info("Find the User object based on userId {}", userId);
		if (userId == null || userId.isEmpty()) {
			logger.info("If userId is null or empty, throw an exception");
			throw new IllegalArgumentException("User Id should not be null or empty");
		} else {
			logger.info("Find the user and convert it to DTO object.");
			ArgueUser argueUser = getArgueUserById(userId);
			return convertToDTO(argueUser);
		}
	}

	public List<UserDTO> getAllUsers() {
		logger.info("Getting of all users objects.");
		List<ArgueUser> listUser = userRepo.findAll();
		logger.info("Converting the list of Argue user to ");
		return listUser.stream().map(user -> convertToDTO(user)).collect(Collectors.toList());
	}

	public UserDTO createArgueUser(@Valid UserDTO userDTO) {
		logger.info("Creating argue user from dto={}", userDTO);
		ArgueUser user = convertToModel(userDTO);
		ArgueUser savedUser = userRepo.save(user);
		return convertToDTO(savedUser);
	}

	public UserDTO updateArgueUpdate(String userId, ArgueUser argueUser) {
		logger.info("Updating the argue user based on userId ={} and data={}", userId, argueUser);
		if (userId == null || userId.isEmpty()) {
			logger.info("If UserId is null and empty, throws an illegal argument exception.");
			throw new IllegalArgumentException("User Id should not be null or empty");
		} else {
			logger.info("if userId={} is valid, get the argue user object and update it", userId);
			ArgueUser user = getArgueUserById(userId);
			if (user != null) {
				UUID id = argueUser.getId();
				if (id == null) {
					argueUser.setId(UUID.fromString(userId));
				}
				ArgueUser updatedUser = userRepo.save(argueUser);
				logger.info("Argue user updated.");
				return convertToDTO(updatedUser);
			} else {
				logger.info("throw exception if userId is not present in the database.");
				throw new IllegalArgumentException("Argue User with following " + userId + " is not available.");
			}
		}
	}

	public void deleteArgueUser(String userId) {
		logger.info("Deleting the argue user object based on argue id={}", userId);
		if (userId == null || userId.isEmpty()) {
			logger.info("If userId is null or empty, throws as exception");
			throw new IllegalArgumentException("User Id should not be null");
		} else {
			logger.info("Fetch the argue user object from userId = {}");
			ArgueUser argueUser = getArgueUserById(userId);
			if (argueUser != null) {
				userRepo.delete(argueUser);
			} else {
				logger.info("throw exception if there is no argue user object related to argueId={} ", userId);
				throw new NoSuchElementException("User with id " + userId + " is not found.");
			}
		}
	}

	private ArgueUser convertToModel(@Valid UserDTO userDTO) {
		ArgueUser argueUser = new ArgueUser();
		BeanUtils.copyProperties(userDTO, argueUser);
		return argueUser;
	}

	private UserDTO convertToDTO(ArgueUser savedUser) {
		UserDTO userDto = new UserDTO();
		BeanUtils.copyProperties(savedUser, userDto);
		return userDto;
	}

	private ArgueUser getArgueUserById(String userId) {
		try {
			UUID uuid = UUID.fromString(userId);
			Optional<ArgueUser> optionalInviteUser = userRepo.findById(uuid);

			if (optionalInviteUser.isPresent()) {
				return optionalInviteUser.get();
			} else {
				logger.info("throw exception if there is no argue user object related to argueId={}", userId);
				throw new NoSuchElementException("User with id " + uuid.toString() + " is not found.");
			}
		} catch (Exception exp) {
			logger.info("throw exception if the userId is not well formed UUID.");
			throw new IllegalArgumentException("Invalid User Id.");
		}

	}
}
