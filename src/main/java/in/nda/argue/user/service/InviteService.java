package in.nda.argue.user.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Base64;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Properties;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.nda.argue.user.config.InviteConfig;
import in.nda.argue.user.dto.InviteUserDTO;
import in.nda.argue.user.exception.RoleException;
import in.nda.argue.user.exception.SendMailException;
import in.nda.argue.user.model.ArgueRole;
import in.nda.argue.user.model.InviteUser;
import in.nda.argue.user.repo.ArgueUserRepository;
import in.nda.argue.user.repo.InviteUserRepository;

@Service
public class InviteService {

	private Logger logger = LoggerFactory.getLogger(InviteService.class);

	@Autowired
	private InviteUserRepository inviteRepository;

	@Autowired
	private ArgueUserRepository userRepository;

	@Autowired
	private InviteConfig config;

	private String mailContents;

	public String sendMailInvitation(InviteUserDTO inviteUserDTO, String url, String userName) {
		logger.info("Get the UserRole from the userName={}", userName);
		String userRole = userRepository.findArgueUserByUserName(userName).getRole();
		String inviteeRole = inviteUserDTO.getRole();
		logger.info("Check the validity of userRole and inviteeRole.");
		checkUserValidity(userRole, inviteeRole);

		try {
			InviteUser inviteUser = convertToInviteUser(inviteUserDTO);
			inviteUser.setInvideTime(LocalDateTime.now());
			logger.info("Save the invite data into repository.");
			InviteUser savedInviteUser = inviteRepository.save(inviteUser);
			String token = Base64.getEncoder().encodeToString(savedInviteUser.getInviteId().toString().getBytes());

			logger.info("Construction of link and message body contents.");
			String strLink = url + token;
			mailContents = mailContents.replace("argueUser", inviteUserDTO.getInvitee());
			mailContents = mailContents.replace("argueLink", strLink);

			logger.info("Filling of message properties.");
			Properties properties = new Properties();
			properties.put("mail.transport.protocol", "smtp");
			properties.put("mail.smtp.host", config.getHostName());
			properties.put("mail.smtp.port", config.getPassword());
			properties.put("mail.smtp.auth", "true");

			Authenticator auth = new SMTPAuthenticator();

			Session mailSession = Session.getDefaultInstance(properties, auth);

			logger.info("Constructing of message body.");
			MimeMessage message = new MimeMessage(mailSession);
			Multipart multipart = new MimeMultipart("alternative");
			BodyPart bodyPart = new MimeBodyPart();
			bodyPart.setContent(mailContents, "text/plain");
			multipart.addBodyPart(bodyPart);
			message.setFrom(new InternetAddress(config.getFrom()));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(inviteUserDTO.getEmail()));
			message.setSubject("Invitation For Registration");
			message.setContent(multipart, MimeMessage.INLINE);

			logger.info("Creating of Transport object to send message to Invitee.");
			Transport transport = mailSession.getTransport();
			transport.connect();
			transport.sendMessage(message, message.getAllRecipients());
			transport.sendMessage(message, new Address[] { new InternetAddress(inviteUserDTO.getEmail()) });
			transport.close();

			return "Mail sent successfully!";
		} catch (Exception exp) {
			logger.info("Exception occurs during sending invitation mail to user. Exception={}", exp.getMessage());
			throw new SendMailException(exp.getMessage());
		}
	}

	private void checkUserValidity(String userRole, String inviteeRole) {
		boolean isTrue = true;
		if (userRole.equalsIgnoreCase(
				ArgueRole.RootAdmin.getAssignRole()) && inviteeRole.equalsIgnoreCase(ArgueRole.ManagementAdmin.getAssignRole())) {
			logger.info("The user with role \"RootAdmin\" can only create user of role \"ManagementAdmin\"");
			isTrue = false;
		}
		if (userRole.equalsIgnoreCase(
				ArgueRole.ManagementAdmin.getAssignRole()) && inviteeRole.equalsIgnoreCase(ArgueRole.Engineer.getAssignRole())) {
			logger.info("The user with role \"ManagementAdmin\" can only create user of role \"Engineer\"");
			isTrue = false;
		}
		if (userRole.equalsIgnoreCase(
				ArgueRole.Engineer.getAssignRole()) && inviteeRole.equalsIgnoreCase(ArgueRole.Operator.getAssignRole())) {
			logger.info("The user with role \"Engineer\" can only create user of role \"Operator\"");
			isTrue = false;
		}
		
		if(isTrue) {
			logger.info("Current user with role={}, can not create user of given role={}", userRole, inviteeRole);
			throw new RoleException();
		}
	}

	@PostConstruct
	public void readMailContent() {
		try {
			logger.info("Reading of mail content from file \"invite.txt\"");
			mailContents = new String(Files.readAllBytes(Paths.get("invite.txt")));
		} catch (IOException exp) {
			logger.info("Exception occurs while reating the invite mail content from file \"invite.txt\"");
		}

	}

	private InviteUser convertToInviteUser(@Valid InviteUserDTO inviteUserDTO) {
		InviteUser inviteUser = new InviteUser();
		BeanUtils.copyProperties(inviteUserDTO, inviteUser);
		return inviteUser;
	}

	public InviteUser getInvitationDetail(String inviteId) {
		return getInviteUserById(inviteId);
	}

	public List<InviteUser> getAllInvites() {
		return inviteRepository.findAll();
	}

	public void deleteInvite(String inviteId) {
		InviteUser inviteUser = getInviteUserById(inviteId);
		if (inviteUser != null) {
			inviteRepository.delete(inviteUser);
		}
	}

	public boolean validateInvite(String token) {
		String inviteId = new String(Base64.getDecoder().decode(token));
		InviteUser inviteUser = getInviteUserById(inviteId);
		LocalDateTime inviteTime = inviteUser.getInviteTime();
		LocalDateTime currentTime = LocalDateTime.now();
		long inviteTimeInSeconds = inviteTime.toEpochSecond(ZoneOffset.UTC);
		long currentTimeInSeconds = currentTime.toEpochSecond(ZoneOffset.UTC);

		long diffInSeconds = currentTimeInSeconds - inviteTimeInSeconds;
		long validDuration = config.getTimeout();
		return (diffInSeconds <= validDuration);
	}

	private InviteUser getInviteUserById(String inviteId) {
		logger.info("Fetching of InviteUser object from inviteId={}", inviteId);
		UUID uuid = UUID.fromString(inviteId);
		Optional<InviteUser> optionalInviteUser = inviteRepository.findById(uuid);

		if (optionalInviteUser.isPresent()) {
			return optionalInviteUser.get();
		} else {
			logger.info("Throw exceptions if there is there is no any object related to UUID={} ", uuid);
			throw new NoSuchElementException("Invitation with id " + uuid + " is not found.");
		}
	}

	private class SMTPAuthenticator extends javax.mail.Authenticator {
		public PasswordAuthentication getPasswordAuthentication() {
			String username = config.getUserName();
			String password = config.getPassword();
			return new PasswordAuthentication(username, password);
		}
	}

	public InviteUser getUserFromToken(String token) {
		logger.info("Fetching of InviteUser object from the token={}", token);
		String inviteId = new String(Base64.getDecoder().decode(token));
		InviteUser inviteUser = getInviteUserById(inviteId);
		return inviteUser;
	}
}
