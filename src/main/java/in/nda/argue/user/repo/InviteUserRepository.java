package in.nda.argue.user.repo;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import in.nda.argue.user.model.InviteUser;

public interface InviteUserRepository extends JpaRepository<InviteUser, UUID>{

}
