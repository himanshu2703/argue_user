package in.nda.argue.user.repo;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import in.nda.argue.user.model.ArgueUser;

public interface ArgueUserRepository extends JpaRepository<ArgueUser, UUID> {
	
	ArgueUser findArgueUserByUserName(String userName);

}
