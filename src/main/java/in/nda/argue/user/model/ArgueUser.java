package in.nda.argue.user.model;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "ARGUE_USER")
public class ArgueUser {

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
	@Column(name = "USER_ID", unique = true)
	private UUID id;

	@Column(name = "FIRST_NAME", length = 25)
	private String firstName;

	@Column(name = "LAST_NAME", length = 25)
	private String lastName;

	@NotNull
	@Column(name = "USER_NAME", length = 25, unique = true)
	private String userName;

	@NotNull
	@Column(name = "ROLE", length = 25)
	private String role;

	@NotNull
	@Column(name = "EMAIL", length = 75)
	private String email;

	@Column(name = "ADDRESS", length = 150)
	private String address;

	@Column(name = "PHONE", length = 75)
	private String phone;

	@Column(name = "DEPARTMENT", length = 75)
	private String department;

	@Column(name = "DATE_TIME", length = 75)
	private LocalDateTime doj;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public LocalDateTime getDoj() {
		return doj;
	}

	public void setDoj(LocalDateTime doj) {
		this.doj = doj;
	}

	@Override
	public String toString() {
		return "ArgueUser [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", userName=" + userName
				+ ", role=" + role + ", email=" + email + ", address=" + address + ", phone=" + phone + ", department="
				+ department + ", doj=" + doj + "]";
	}

}
