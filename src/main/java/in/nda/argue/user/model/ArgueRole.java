package in.nda.argue.user.model;

import javax.persistence.Embeddable;

@Embeddable
public enum ArgueRole {
	RootAdmin("RootAdmin"), ManagementAdmin("Management"), Engineer("Engineer"), Operator("Operator");
	
	private String assignRole;
	private ArgueRole(String assignRole) {
		this.assignRole = assignRole; 
	}
	
	public String getAssignRole() {
		return assignRole;
	}
}
