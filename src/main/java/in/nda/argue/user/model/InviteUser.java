package in.nda.argue.user.model;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="INVITE_USER")
public class InviteUser {
	
	@Id
	@GeneratedValue(generator = "invite_uuid")
	@GenericGenerator(name = "invite_uuid", strategy = "org.hibernate.id.UUIDGenerator")
	@Column(name = "INVITE_ID", unique = true)
	private UUID inviteId;
	
	@Column(name = "INVITEE", length = 75)
	private String invitee;
	
	@Column(name = "EMAIL", length = 75)
	private String email;
	
	@Column(name = "INVITE_TIME")
	private LocalDateTime inviteTime;

	public UUID getInviteId() {
		return inviteId;
	}

	public void setInviteId(UUID inviteId) {
		this.inviteId = inviteId;
	}

	public String getInvitee() {
		return invitee;
	}

	public void setInvitee(String invitee) {
		this.invitee = invitee;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public LocalDateTime getInviteTime() {
		return inviteTime;
	}

	public void setInvideTime(LocalDateTime inviteTime) {
		this.inviteTime = inviteTime;
	}

	@Override
	public String toString() {
		return "InvideUser [inviteId=" + inviteId + ", invitee=" + invitee + ", email=" + email + ", invideTime="
				+ inviteTime + "]";
	}

}
