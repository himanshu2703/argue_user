package in.nda.argue.user.mvc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import in.nda.argue.user.dto.InviteUserDTO;
import in.nda.argue.user.service.InviteService;

@CrossOrigin
@Controller
@RequestMapping(value = "/mvc")
public class ArgueWebController {
	
	private Logger logger = LoggerFactory.getLogger(ArgueWebController.class);
	
	@Autowired
	private InviteService inviteService;
	
	@GetMapping(value= {"/","/dashboard"})
    public String dashBoard(Model model) {
		logger.info("Sending dashboard page to the user.");
        return "dashboard";
    }
	
	
	@GetMapping("/invite")
    public String invitePage(Model model) {
		logger.info("Sending invite page to the user.");
		model.addAttribute("message", "");
		model.addAttribute("inviteDto", new InviteUserDTO());
        return "invite";
    }
	
	@PostMapping("/invite")
    public String invitePage(@ModelAttribute InviteUserDTO inviteDto, Model model) {		
		try {
			logger.info("Creating of link for registeration.");
			ControllerLinkBuilder linkBuilder = ControllerLinkBuilder.linkTo(ArgueWebController.class)
					.slash("registration");
			String strLink = linkBuilder.toUri().toString() + "/";
			String userName = "abcHimanshu";
			logger.info("Sending invitation mail to user={}.", userName);
			String strMessage = inviteService.sendMailInvitation(inviteDto, strLink, userName);
			model.addAttribute("message", strMessage);
		} catch(Exception exp) {
			logger.info("Exception occurs while sending invitation to user: Exception: {}.", exp.getMessage());
		}
		model.addAttribute("inviteDto", new InviteUserDTO());
        return "invite";
    }
	
	@GetMapping("/userinfo")
    public String getUserInfo(Model model) {
		logger.info("Sending of userInfo page to the web.");
        return "userInfo";
    }
	
	@GetMapping(value="/registration/{token}")
    public String registrationUser(Model model,@PathVariable String token) {
		logger.info("Validating the token of invite.");
		boolean isValidInvite = inviteService.validateInvite(token);
		
		logger.info("if token is valid send registration form otherwise send message of token expire.");
		String statusMessage =  isValidInvite? "Aao Kabhi Haveli Pe" : "Invitation has expired";
		model.addAttribute("message", statusMessage);
        return "register";
    }

}
