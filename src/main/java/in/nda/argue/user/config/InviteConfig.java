package in.nda.argue.user.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:argue.properties")
public class InviteConfig {

	@Value( "${argue.user.invite.timeout}" )
	private int timeout;
	
	@Value( "${argue.mail.smtp.username}" )
	private String userName;
	
	@Value( "${argue.mail.smtp.password}" )
	private String password;
	
	@Value( "${argue.mail.smtp.host}" )
	private String hostName;
	
	@Value( "${argue.mail.smtp.port}" )
	private String port;
	
	@Value( "${argue.mail.smtp.key}" )
	private String key;
	
	@Value( "${argue.mail.smtp.from}" )
	private String from;

	public int getTimeout() {
		return timeout;
	}

	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}
	

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	@Override
	public String toString() {
		return "InviteConfig [timeout=" + timeout + ", userName=" + userName + ", password=" + password + ", hostName="
				+ hostName + ", port=" + port + ", key=" + key + ", from=" + from + "]";
	}

}
